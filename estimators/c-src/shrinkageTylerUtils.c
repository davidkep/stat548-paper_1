/*
 * shrinkageTylerUtils.c
 *
 * Created by David Kepplinger on 2015-09-18.
 * Copyright (c) 2015 David Kepplinger. All rights reserved.
 *
 * This file is part of shrinkageTyler.
 *
 * shrinkageTyler is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version.
 *
 * rrlda2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shrinkageTyler. If not, see <http://www.gnu.org/licenses/>.
 */

#include "shrinkageTylerUtils.h"

#include <R.h>
#include <Rmath.h>
#include <Rinternals.h>

#include <string.h>

#include "linAlg.h"

static const double BLAS_1F = 1.0;
static const int BLAS_1I = 1;

static const char * const BLAS_TRANS_YES = "T";
static const char * const BLAS_TRANS_NO = "N";
static const char * const BLAS_DIAG_NON_UNIT_TRI = "N";
static const char * const BLAS_DIAG_UNIT_TRI = "U";
static const char * const BLAS_LEFT = "L";
static const char * const BLAS_UPLO_UP = "U";


#ifdef DEBUG_MESSAGES
static void print_matf(int dr, int dc, const double *A) {
    int r, c, i = 0;
    for (r = 0; r < dr; ++r) {
        for (c = 0, i = r; c < dc; ++c, i += dr) {
            Rprintf("%8.4f ", A[i]);
        }
        Rprintf("\n");
    }
}
#endif

int doTylerStep(double *restrict current, const double *restrict data, const int n,
                       const int p, const double rho, double *restrict updated, double *restrict bufVec) {
    double constScale = (1 - rho) * (double) p / (double) n;
    int numEls = p * p;
    int i;
    /* int j, counterLo, counterUp; */
    double scale;
    double *restrict tmp;
    int lapackInfo;

    /* First calculate the Cholesky factorization of the current estimate */
    LAPACK_DPOTRF(BLAS_UPLO_UP, p, current, p, &lapackInfo);

#ifdef DEBUG_MESSAGES
    Rprintf("Cholesky factorization:\n");
    print_matf(p, p, current);
#endif

    if (lapackInfo != 0) {
        return lapackInfo;
    }

    /* reset updated with all 0s */
    memset(updated, 0, sizeof(double) * numEls);

    for (i = 0; i < n; ++i, data += p) {
        /* First copy the `s` vector into the buffer */
        memcpy(bufVec, data, sizeof(double) * p);

        /* Calculate the scaling factor using the Cholesky factorization A = L.L^T and the fact that
        * x^T . A^-1 . x = x . L^-1 . L^-T . x = (L^-T . x)^T . (L^-T . x)^T
        */
        BLAS_DTRSV(BLAS_UPLO_UP, BLAS_TRANS_YES, BLAS_DIAG_NON_UNIT_TRI, p, current, p, bufVec, BLAS_1I);
        scale = 1 / BLAS_DDOT(p, bufVec, BLAS_1I, bufVec, BLAS_1I);

        /* Now add the outer product, scaled by `scale`, to the buffer matrix */
        BLAS_DSYR(BLAS_UPLO_UP, p, scale, data, BLAS_1I, updated, p);
    }

    /* Now scale the matrix by the constant factor */
    BLAS_DSCAL(numEls, constScale, updated, BLAS_1I);

    if (rho > 0) {
        /* Calculate the trace and add the rho to the diagonal elements in a single run */
        scale = 0;
        for (i = 0, tmp = updated; i < p; ++i, tmp += (p + 1)) {
            *tmp += rho;
            scale += *tmp;
        }

        scale = (double) p / scale;

        /* Rescale the matrix again */
        BLAS_DSCAL(numEls, scale, updated, BLAS_1I);
    }

#ifdef DEBUG_MESSAGES
    Rprintf("\nScaling factor for estimate: %.5f\n", scale);
    Rprintf("Estimate after the step:\n");
    print_matf(p, p, updated);
#endif

    return 0;
}

double frobeniusNormDiffAndMirror(const double *restrict a, double *restrict b, double *restrict aDiag,
                                  const int p) {
    double sumSqDiff = 0;
    const double *restrict aColStart = a;
    double tmp;

    int i, j, offset;
    int vLength = p;

#ifdef DEBUG_MESSAGES
    Rprintf("Original diagonal elements:\n");
    print_matf(1, p, aDiag);
#endif

    for (i = 1; i <= p; ++i, a += p + 1, b += p + 1, aColStart += p, ++aDiag) {
        /* `aColStart` points to the first row in the i'th column of a */
        /* `a` and `b` point to their i'th diagonal element */

        /* Diagonal element is given as an extra parameter */
        tmp = (*aDiag) - *b;
        sumSqDiff += tmp * tmp;

        for (j = 1, offset = p; j < vLength; ++j, offset += p) {
            /* Copy value from upper triangular part to lower triangular part */
            *(b + j) = *(b + offset);

            /* Add to sum of squared differences */
            tmp = *(a + j) - *(b + offset);
            sumSqDiff += tmp * tmp;
        }

        /* The next column vector of the lower diagonal is shorter by one */
        --vLength;
    }

    return sumSqDiff;
}


void centerNormalizeAndTranspose(const double *restrict original, double *restrict transformed,
                                 const int n, const int p, const double *restrict centers) {
    int i, j, origOffset;
    double scale;

    for (i = 0; i < n; ++i, ++original, transformed += p) {
        scale = 0;
        for (j = 0, origOffset = 0; j < p; ++j, origOffset += n) {
            *(transformed + j) = *(original + origOffset) - *(centers + j);
            scale += (*(transformed + j)) * (*(transformed + j));
        }

        scale = 1 / sqrt(scale);

        BLAS_DSCAL(p, scale, transformed, BLAS_1I);
    }
}

