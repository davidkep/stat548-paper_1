/*
 * linAlg.h
 *
 * Created by David Kepplinger on 2015-09-18.
 * Copyright (c) 2015 David Kepplinger. All rights reserved.
 *
 * This file is part of shrinkageTyler.
 *
 * shrinkageTyler is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version.
 *
 * rrlda2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shrinkageTyler. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LINALG_h
#define LINALG_h

#include <R_ext/BLAS.h>
#include <R_ext/Lapack.h>

#define LAPACK_DPOTRF(uplo, n, a, lda, info)                    								\
	F77_CALL(dpotrf)(uplo, &n, a, &lda, info)

#define BLAS_DTRSV(uplo, trans, diag, n, a, lda, x, incx)										\
	F77_CALL(dtrsv)(uplo, trans, diag, &n, a, &lda, x, &incx)

#define BLAS_DSYR(uplo, n, alpha, x, incx, a, lda)												\
	F77_CALL(dsyr)(uplo, &n, &alpha, x, &incx, a, &lda)

#define BLAS_DDOT(n, x, incx, y, incy)															\
	F77_CALL(ddot)(&n, x, &incx, y, &incy)

#define BLAS_DSCAL(n, alpha, x, incx)															\
    F77_CALL(dscal)(&n, &alpha, x, &incx)

#define BLAS_DCOPY(n, x, incx, y, incy)															\
    F77_CALL(dcopy)(&n, x, &incx, y, &incy)

#define BLAS_DNRM2(n, x, incx)	        														\
    F77_CALL(dnrm2)(&n, x, &incx)


#endif /* BLAS_h */
