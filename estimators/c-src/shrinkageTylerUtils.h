/*
 * shrinkageTylerUtils.h
 *
 * Created by David Kepplinger on 2015-09-18.
 * Copyright (c) 2015 David Kepplinger. All rights reserved.
 *
 * This file is part of shrinkageTyler.
 *
 * shrinkageTyler is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version.
 *
 * rrlda2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shrinkageTyler. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SHRINKAGE_TYLER_UTILS_H
#define SHRINKAGE_TYLER_UTILS_H

/**
* Do a single step in Tyler's fix point iteration with shrinkage
*
* WARNING: No checks on the input will be performed in this function! Be careful and
* make sure all variables point to valid and large enough regions in memory.
*
* @param current The current estimate of the (p x p) covariance matrix
* @param data The scaled and centered observations (transposed) as a (p x n) matrix
* @param p The number of columns in `data`
* @param n The number of rows in `data`
* @param rho The shrinkage parameter
* @param updated A buffer matrix of size (p x p)
* @param bufVec A buffer vector of size (p x 1)
*/
int doTylerStep(double *restrict current, const double *restrict data, const int n,
                       const int p, const double rho, double *restrict updated, double *restrict bufVec);

/**
* Calculate the Frobenius norm of the difference between matrices `a` and `b` and simultaniously
* mirror the upper triangular part of `b` to its lower triangular part.
*
* @param a A (p x p) matrix where the lower triangle is the original matrix
*              and the upper triangle (incl. diagonal) is it's Cholesky decomposition
* @param b A (p x p) upper triangular matrix
* @param aDiag The original diagonal elements of matrix `a` (i.e., a vector with p elements)
* @param p The number of colums and rows in both square matrices `a` and `b`
*/
double frobeniusNormDiffAndMirror(const double *restrict a, double *restrict b, double *restrict aDiag,
                                  const int p);


/**
 * Center, scale, and transpose a matrix
 *
 * @param original The original (n x p) matrix
 * @param transformed A pointer to the memory where the centered, scaled, and transposed (p x n)
 *          matrix will be stored
 * @param n The number of rows in `original`
 * @param p The number of colums in `original`
 * @param centers Vector of p centers for the respective columns in `original`
 */
void centerNormalizeAndTranspose(const double *restrict original, double *restrict transformed,
                                 const int n, const int p, const double *restrict centers);

#endif
