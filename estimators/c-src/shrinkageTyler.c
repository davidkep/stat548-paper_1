/*
 * shrinkageTyler.c
 *
 * Created by David Kepplinger on 2015-09-18.
 * Copyright (c) 2015 David Kepplinger. All rights reserved.
 *
 * This file is part of shrinkageTyler.
 *
 * shrinkageTyler is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version.
 *
 * rrlda2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with shrinkageTyler. If not, see <http://www.gnu.org/licenses/>.
 */

#include <R.h>
#include <Rinternals.h>
#include <R_ext/Rdynload.h>

#include <string.h>

#include "linAlg.h"
#include "shrinkageTylerUtils.h"

static const int BLAS_1I = 1;

/**
 * Perform Tyler's fixed point iterations with shrinkage
 *
 * @param Rdata The (n x p) numeric data matrix
 * @param RcolCenters The p centers of the p covariates (double)
 * @param Rn The number of columns in `RdataT` (integer)
 * @param Rp The number of rows in `RdataT` (integer)
 * @param Rrho The numerical shrinkage parameter (double)
 * @param Rtol The tolerance for convergence (double)
 * @param Rmaxit The maximum number of iterations (integer)
 * @param RrelChange (OUTPUT) The actual relative change before convergence (double)
 * @param Riterations (OUTPUT) The actual number of iterations (integer)
 */
SEXP C_tylerShrinkage(SEXP Rdata, SEXP RcolCenters, SEXP Rn, SEXP Rp, SEXP Rrho,
                      SEXP Rtol, SEXP Rmaxit, SEXP RrelChange, SEXP Riterations) {
	int n = *INTEGER(Rn);
	int p = *INTEGER(Rp);
	int i, nElements = p * p;
	double rho = *REAL(Rrho);
	double tol = *REAL(Rtol);
	int maxit = *INTEGER(Rmaxit);

	double *restrict bufVec = (double*) R_alloc(p, sizeof(double));
	double *restrict diagBuf = (double*) R_alloc(p, sizeof(double));
	double *restrict tmp;

	SEXP Rcurrent = PROTECT(allocMatrix(REALSXP, p, p));
	SEXP Rupdated = PROTECT(allocMatrix(REALSXP, p, p));
	double *restrict current = REAL(Rcurrent);
	double *restrict updated = REAL(Rupdated);
	double *restrict data = (double*) R_alloc(n * p, sizeof(double));

	int* itcount = INTEGER(Riterations);
	double* relChange = REAL(RrelChange);
	int retVal;

	*itcount = 1;

	/*
	 * The first step is to center and normalize the data as well as transposing it
	 * for faster access during the iterations
	 */
	centerNormalizeAndTranspose(REAL(Rdata), data, n, p, REAL(RcolCenters));

	/* We initialize the estimate to the identity matrix */
	memset(current, 0, nElements * sizeof(double));
	for (i = 0; i < nElements; i += p + 1) {
	    *(current + i) = 1;
	}

	/* The diagonal of the initial estimate is all 1's */
	memset(diagBuf, 1, p * sizeof(double));

	i = p + 1; /* This variable is now used as the increment for copying the diagonal elements */

	do {
	    retVal = doTylerStep(current, data, n, p, rho, updated, bufVec);

        if (retVal != 0) {
	        Rprintf("Iteration step returned error code %d\n", retVal);
            *itcount = -1;
            break;
        }

        *relChange = frobeniusNormDiffAndMirror(current, updated, diagBuf, p);

#ifdef DEBUG_MESSAGES
        Rprintf("Relative change in matrix norm after %d. step is %.5f\n", *itcount, *relChange);
#endif
        /* Copy diagonal elements for later use in the calculation of the norm of the difference */
        BLAS_DCOPY(p, updated, i, diagBuf, BLAS_1I);

	    /* Flip pointers of current and updated */
	    tmp = updated;
	    updated = current;
	    current = tmp;

	    /* Check if the user interrupted the iteration */
	    R_CheckUserInterrupt();

	} while (((*itcount)++ < maxit) && (*relChange > tol));

	UNPROTECT(2);

	/**
	 * If we iterated the above loop an even number of times, the final estimate is
	 * stored in the memory of Rupdated, otherwise in the memory of Rcurrent
	 */
	if (((*itcount)-- % 2) != 0) {
	    return Rcurrent;
	}
	return Rupdated;
}

/**
 * Function to register the C function with R for faster search times
 */
void R_init_shrinkageTyler(DllInfo *info) {
    static R_CallMethodDef callMethods[]  = {
        {"C_tylerShrinkage", (DL_FUNC) &C_tylerShrinkage, 9},
        {NULL, NULL, 0}
    };

    R_registerRoutines(info, NULL, callMethods, NULL, NULL);
}

