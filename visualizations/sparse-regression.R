library(ggplot2)
library(reshape2)
library(dplyr)

source("extlib/ggplot-utils.R")

## @knitr setup-sparse-regression
load("output/sparse-regression.rda")

colorSelection <- c(
    "Reg. Tyler" = "blue",
    "LASSO" = "orange")

names(b) <- c("(Intercept)", paste("X", seq_len(length(b) - 1), sep = ""))


## @knitr sparse-regression-bias
# make height 3.5!

##
## extract coefficients
##
coefs <- lapply(regres, function(x) {
    lapply(x, function(y) y$coefs )
})

coefsm <- melt(coefs)
colnames(coefsm) <- c("coef", "Method", "value", "repl", "n")
coefsm$n <- N[coefsm$n]
coefsm$bias <- coefsm$value - b

coefsm$Method <- factor(coefsm$Method, levels = c("tyler", "lasso"),
                        labels = c("Reg. Tyler", "LASSO"))

bsparse <- (b < 1e-9)
##
## Coefficient stats
##
coefStats <- filter(coefsm, coef != "(Intercept)") %>%
    group_by(Method, n, coef) %>%
    summarize(mbias = mean(bias), sqbias = mean(bias^2),
              sparseness = mean(bsparse[coef] & (abs(value) < 1e-9))) %>%
    summarize(Bias = sqrt(mean(mbias^2)), MSE = mean(sqbias),
              Sparseness = sum(sparseness))

coefStats <- melt(coefStats, id.vars = c("Method", "n"), variable.name = "Measure")

legend <- guide_legend(label.position = "left",
                       label.hjust = 1)

ggplot(filter(coefStats, Measure != "Sparseness"), aes(x = n, y = value, color = Method, shape = Method)) +
    geom_line(size = PLOT_LINE_WIDTH) +
    geom_point(size = PLOT_POINT_SIZE) +
    scale_color_colorblind(selection = colorSelection, guide = legend) +
    facet_grid(. ~ Measure, scales = "free") +
    coord_cartesian(ylim = c(0, 0.45), xlim = c(0, 230)) +
    ylab(NULL) +
    ggplot_theme() +
    theme(
        legend.position = c(1.021, 1.055),
        legend.justification = c(1, 1),
        legend.title = element_blank()
    )


## @knitr sparse-regression-mspe

##
## extract MSPE
##
mspes <- lapply(regres, function(x) {
    lapply(x, function(y) as.matrix(y$MSPE) )
})

mspesm <- melt(mspes)
mspesm <- mspesm[ , -2L]
colnames(mspesm) <- c("Method", "MSPE", "repl", "n")
mspesm$n <- N[mspesm$n]
mspesm$Method <- factor(mspesm$Method, levels = c("tyler", "lasso"),
                        labels = c("Reg. Tyler", "LASSO"))


##
## MSPE
##
mspeStats <- group_by(mspesm, Method, n) %>%
    summarize(mMSPE = median(MSPE),
              lMSPE = quantile(MSPE, 0.25),
              uMSPE = quantile(MSPE, 0.75))

legend <- guide_legend(label.position = "left",
                       label.hjust = 1)

ggplot(mspeStats, aes(x = n, y = mMSPE, ymin = lMSPE, ymax = uMSPE, color = Method, fill = Method, shape = Method)) +
    geom_ribbon(alpha = 0.2, color = NA) +
    geom_line(size = PLOT_LINE_WIDTH) +
    geom_point(size = PLOT_POINT_SIZE) +
    scale_fill_colorblind(selection = colorSelection, guide = legend) +
    scale_color_colorblind(selection = colorSelection, guide = legend) +
    coord_cartesian(ylim = c(1, 450), xlim = c(0, 230)) +
    ylab("MSPE") +
    ggplot_theme() +
    theme(
        legend.position = c(1.021, 1.055),
        legend.justification = c(1, 1),
        legend.title = element_blank()
    )
