covTylerRImpl <- function(x, rho, tol = 1e-12, maxIt = 1000L, center) {
    ##
    ## Check arguments
    ##
    if (!is.numeric(x)) {
        stop("`x` must be numeric")
    }

    if (anyNA(x)) {
        stop("Missing values are not yet supported")
    }

    if (!is.numeric(rho) | (length(rho) > 1) | is.na(rho) | identical(rho < 0 | rho > 1, TRUE)) {
        stop("`rho` must be a single number between 0 and 1")
    }

    if (!is.numeric(tol) | (length(tol) > 1) | is.na(tol) | identical(tol <= 0, TRUE)) {
        stop("`tol` must be a single number greater than 0")
    }

    if (!is.numeric(maxIt) | (length(maxIt) > 1) | is.na(maxIt) | identical(maxIt <= 0, TRUE)) {
        stop("`maxIt` must be a single number greater than 0")
    }

    maxIt <- as.integer(maxIt);

    if (missing(center)) {
        center <- "mean"
    }

    # dim(.) is returning integers, so no need to transform them
    dd <- dim(x)
    n <- dd[1]
    p <- dd[2]


    ##
    ## Calculate column centers
    ##
    if (is.character(center)) {
        colCenters <- switch(match.arg(center, choices = c("mean", "median")),
                             mean = .colMeans(x, n, p),
                             median = apply(x, 2, median))

    } else if (is.numeric(center)) {
        if ((length(center) != 1L) && (length(center) != p)) {
            stop("If supplying the centers manually, they must be of length 1 or `p`")
        }
        colCenters <- rep(center, length = p)
    } else {
        stop("`center` must be either a string or a numeric vector")
    }

    xc <- scale(x, center = colCenters, scale = FALSE)
    xt <- apply(xc, 1, function(xi) {
        xi / sqrt(crossprod(xi))[1L]
    })

    ##
    ## Set up output parameters
    ##
    relChange <- NA_real_;
    nIter <- 0L;

    ##
    ## Actual algorithm
    ##
    newEst <- diag(p)
    oldEst <- newEst
    const <- (1 - rho) * p / n

    nseq <- seq_len(n)[-1]

    repeat {
        oldEst <- newEst

        oldInv <- solve(oldEst)

        # Calculate estimate
        denom <- crossprod(xt[, 1L], oldInv) %*% xt[ , 1L]
        newEst <- tcrossprod(xt[ , 1L]) / denom[1L]

        for (i in nseq) {
            denom <- crossprod(xt[ , i], oldInv) %*% xt[ , i]
            newEst <- newEst + tcrossprod(xt[ , i]) / denom[1L]
        }

        newEst <- const * newEst
        diag(newEst) <- rho + diag(newEst)

        # Normalizing
        if (rho > 0) {
            newEst <- newEst * (p / sum(diag(newEst)))
        }

        # Check for stopping condition
        nIter <- nIter + 1L;

        relChange <- sum((newEst - oldEst)^2)

        if ((relChange < tol) || (nIter >= maxIt)) {
            break;
        }
    }

    ##
    ## Check status
    ##
    if (nIter < 0) {
        stop("The Cholesky factorization of a intermediate estimate failed.")
    } else if (nIter >= maxIt) {
        warning("The algorithm did not converge within the allowed number of iterations. ",
                sprintf("The change between iterations is still %.8f", relChange),
                "Consider increasing the number of iterations or the tolerance.")
    }

    attr(newEst, "relChange") <- relChange;
    attr(newEst, "nIter") <- nIter;

    return(newEst);
}
