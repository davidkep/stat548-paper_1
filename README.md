# STAT 548 – Paper 1 #
## Regularized Tyler's shape estimator for high-dimensional covariance matrices ##

This repository hosts an R implementation of the robust shrinkage covariance estimator proposed
in Yilun Chen, Ami Wiesel, and Alfred O. Hero (2011) Robust Shrinkage Estimation of High-Dimensional
Covariance Matrices. IEEE Transactions on Signal Processing 59, 4097:4107.

In addition to the regularized Tyler's shape estimator, an R implementation of the
Ledoit-Wolf estimator (Ledoit, O. and Wolf, M. (2003),
Improved estimation of the covariance matrix of stock returns with an application to portfolio selection.
Journal of Empirical Finance 10, 603-621.) based on their
[Matlab reference implementation](http://www.econ.uzh.ch/en/people/faculty/wolf/publications.html#9)
is provided.

The directory [extlib/](extlib/) further contains the (yet) unpublished R package _rrlda2_ which
includes functions to calculate the [robust glasso](http://media.obvsg.at/p-AC12315465-2001)
estimator.

## Installation ##
The regularized Tyler's shape estimator is implemented in C and needs to be compiled for R first.
A Makefile is provided under [estimators/c-src/](./estimators/c-src/) for *NIX based systems,
including Mac OS X and Linux derivatives. Under Windows,
[Rtools](https://cran.r-project.org/bin/windows/Rtools/) is
needed for compilation. To compile the shared library on Windows, open the command line and
navigate to the [estimators/c-src/](./estimators/c-src/) directory. There, execute
```bat
R.exe CMD SHLIB -o shrinkageTyler.so shrinkageTyler.c shrinkageTylerUtils.c
```

For the simulations to run, the source package `rrlda2_1.1.1.tar.gz` in [extlib/](extlib/) must
be compiled separately by executing
```r
install.packages("extlib/rrlda2_1.1.1.tar.gz", repos = NULL)
```
inside R.

## Usage ##
Once the shared library is built, the estimators can be loaded with
```r
source("estimators/estimators.R", chdir = TRUE)
```
It is important to set `chdir = TRUE` such that the shared library can
be located.

The three main functions for estimating the covariance matrix are
```r
tylerRho()
covTyler()
covLedoitWolf()
```
For help on how to use these functions, see the documentation in the respective source
file in the directory [estimators/](./estimators/).

The [simulations/](./simulations/) directory contains some numerical simulations comparing the different
estimators and under [visualizations/](./visualizations/) the simulation results are visualized. Note that
the files in [visualizations/](./visualizations/) require that the simulations were run before and the
outputs have been stored in [output/](./output/), which is done by default.

To check the correctness of the C code, an R implementation of the regularized
Tyler's shape estimator is also made available in the [tests/](./tests/) directory. The
results of the C and the R implementation should in general be identical (plus/minus
small differences to to numerical inaccuracies).

## License ##
The code is provided under the GNU GPL v3, for more informations see [LICENSE.txt](./LICENSE.txt).
The R package _rrlda2_ is licensed under GNU GPL v2 and license information is included
in the package.
